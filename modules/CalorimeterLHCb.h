/*
 *  Delphes: a framework for fast simulation of a generic collider experiment
 *  Copyright (C) 2012-2014  Universite catholique de Louvain (UCL), Belgium
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CalorimeterLHCb_h
#define CalorimeterLHCb_h

/** \class CalorimeterLHCb
 *
 *  Fills calorimeter towers, performs calorimeter resolution smearing,
 *  and creates energy flow objects (tracks, photons, and neutral hadrons).
 *
 *  \author Zehua Xu - CN, Tsinghua University 
 *  \author Adam Davis - University of Manchester
 */

#include "classes/DelphesModule.h"

#include <map>
#include <set>
#include <vector>

class TObjArray;
class DelphesFormula;
class Candidate;

class CalorimeterLHCb: public DelphesModule
{
public:

  CalorimeterLHCb();
  ~CalorimeterLHCb();

  void Init();
  void Process();
  void Finish();

  std::vector<double> GetXBorders(){ return BorderX; }
  std::vector<double> GetYBorders(){ return BorderY; }
  
    
private:

  typedef std::map< Long64_t, std::pair< Double_t, Double_t > > TFractionMap; //!
  typedef std::map< Double_t, std::set< Double_t > > TBinMap; //!

  Candidate *fTower;
  Double_t fTowerX, fTowerY, fTowerEta, fTowerEdges[4];
  Double_t fECalTowerEnergy, fHCalTowerEnergy;
  Double_t fECalTrackEnergy, fHCalTrackEnergy;





  Double_t fTimingEnergyMin;
  Bool_t fElectronsFromTrack;

  Int_t fTowerTrackHits, fTowerPhotonHits;

  Double_t fECalEnergyMin;
  Double_t fHCalEnergyMin;

  Double_t fECalEnergySignificanceMin;
  Double_t fHCalEnergySignificanceMin;

  Double_t fECalTrackSigma;
  Double_t fHCalTrackSigma;

  Bool_t fSmearTowerCenter;

  TFractionMap fFractionMap; //!
  TBinMap fBinMap; //!

  std::vector < Double_t > fXBins;
  std::vector < std::vector < Double_t >* > fYBins;

// Add new numbers // Zehua Xu
  std::vector <Double_t > BorderX;
  std::vector <Double_t > BorderY;

  std::vector < TBinMap > Vec_Map;
  std::vector < std::vector < Double_t > > xtotBins;
  std::vector < std::vector < std::vector < Double_t>* > > ytotBins;


//////////////////////////////////////



  std::vector < Long64_t > fTowerHits;

  std::vector < Double_t > fECalTowerFractions;
  std::vector < Double_t > fHCalTowerFractions;

  std::vector < Double_t > fECalTrackFractions;
  std::vector < Double_t > fHCalTrackFractions;

  DelphesFormula *fECalResolutionFormula; //!
  DelphesFormula *fHCalResolutionFormula; //!

  TIterator *fItParticleInputArray; //!
  TIterator *fItTrackInputArray; //!

  const TObjArray *fParticleInputArray; //!
  const TObjArray *fTrackInputArray; //!

  TObjArray *fTowerOutputArray; //!
  TObjArray *fPhotonOutputArray; //!

  TObjArray *fEFlowTrackOutputArray; //!
  TObjArray *fEFlowPhotonOutputArray; //!
  TObjArray *fEFlowNeutralHadronOutputArray; //!

  TObjArray *fECalTowerTrackArray; //!
  TIterator *fItECalTowerTrackArray; //!

  TObjArray *fHCalTowerTrackArray; //!
  TIterator *fItHCalTowerTrackArray; //!


  double m_zEcal;//z position of the electromagnetic calo 
  std::string m_head{ "CalorimeterLHCb:\t"};
  
  void FinalizeTower();
  Double_t LogNormal(Double_t mean, Double_t sigma);
  bool m_debug;//debug flag  
  ClassDef(CalorimeterLHCb, 1)
};

#endif
