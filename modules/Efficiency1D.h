/*
 *  Delphes: a framework for fast simulation of a generic collider experiment
 *  Copyright (C) 2012-2014  Universite catholique de Louvain (UCL), Belgium
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef Efficiency1D_h
#define Efficiency1D_h

/** \class Efficiency1D
 *
 *  Selects candidates from the InputArray according to the efficiency formula.
 *
 *  \author B. Siddi - INFN Ferrara/CERN (2015-2017)
 *
 */

#include "classes/DelphesModule.h"

#include "TFile.h"
#include "TH3.h"
#include "TH2.h"

class TIterator;
class TObjArray;
class DelphesFormula;

class Efficiency1D: public DelphesModule
{
public:

  Efficiency1D();
  ~Efficiency1D();

  void Init();
  void Process();
  void Finish();

private:
  const char *fFileHistoName; //!
  const char *fHisto_num; //!
  const char *fHisto_den; //!
  double r;
  

  TH1D *fhNum;
  TH1D *fhDen;
  TH1D *fhEff;
  
  TH1D *fhEff_hx;
  TH1D *fhEff_hy;
  TH1D *fhEff_hz;
  TH1D *fhDenominator_hx;
  TH1D *fhDenominator_hy;
  TH1D *fhDenominator_hz;
  

  TFile *fFileHisto;

  TIterator *fItInputArray; //!

  const TObjArray *fInputArray; //!

  TObjArray *fOutputArray; //!

  ClassDef(Efficiency1D, 1)
};

#endif
