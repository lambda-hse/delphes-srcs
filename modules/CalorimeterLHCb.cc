/*
 *  Delphes: a framework for fast simulation of a generic collider experiment
 *  Copyright (C) 2012-2014  Universite catholique de Louvain (UCL), Belgium
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/** \class Calorimeter
 *
 *  Fills calorimeter towers, performs calorimeter resolution smearing,
 *  and creates energy flow objects (tracks, photons, and neutral hadrons).
 *
 *  \author \change it for LHCb Zehua Xu , Tsinghua University , E-mail: Zehua.Xu@cern.ch
 */

#include "modules/CalorimeterLHCb.h"

#include "classes/DelphesClasses.h"
#include "classes/DelphesFactory.h"
#include "classes/DelphesFormula.h"

#include "ExRootAnalysis/ExRootResult.h"
#include "ExRootAnalysis/ExRootFilter.h"
#include "ExRootAnalysis/ExRootClassifier.h"

#include "TMath.h"
#include "TString.h"
#include "TFormula.h"
#include "TRandom3.h"
#include "TObjArray.h"
#include "TDatabasePDG.h"
#include "TLorentzVector.h"

#include <algorithm>
#include <stdexcept>
#include <iostream>
#include <sstream>

using namespace std;

//------------------------------------------------------------------------------

CalorimeterLHCb::CalorimeterLHCb() :
  fECalResolutionFormula(0), fHCalResolutionFormula(0),
  fItParticleInputArray(0), fItTrackInputArray(0)
{
  
  fECalResolutionFormula = new DelphesFormula;
  fHCalResolutionFormula = new DelphesFormula;

  fECalTowerTrackArray = new TObjArray;
  fItECalTowerTrackArray = fECalTowerTrackArray->MakeIterator();

  fHCalTowerTrackArray = new TObjArray;
  fItHCalTowerTrackArray = fHCalTowerTrackArray->MakeIterator();
  
}

//------------------------------------------------------------------------------

CalorimeterLHCb::~CalorimeterLHCb()
{
  
  if(fECalResolutionFormula) delete fECalResolutionFormula;
  if(fHCalResolutionFormula) delete fHCalResolutionFormula;

  if(fECalTowerTrackArray) delete fECalTowerTrackArray;
  if(fItECalTowerTrackArray) delete fItECalTowerTrackArray;

  if(fHCalTowerTrackArray) delete fHCalTowerTrackArray;
  if(fItHCalTowerTrackArray) delete fItHCalTowerTrackArray;
 
}

//------------------------------------------------------------------------------

void CalorimeterLHCb::Init()
{
  ExRootConfParam param, paramXBins, paramYBins, paramFractions;
  ExRootConfParam paramBorderX, paramBorderY;
  Long_t i, j, k, size, sizeXBins, sizeYBins;
  Double_t ecalFraction, hcalFraction;
  TBinMap::iterator itXBin;
  set< Double_t >::iterator itYBin;
  vector< Double_t > *YBins;
  m_debug = GetInt("Debug",0);
  
  if(m_debug)std::cout<<m_head<<"===============Init CalorimeterLHCb==============="<<std::endl;
  BorderX.clear();
  BorderY.clear();
  // read the X and Y border vector
  paramBorderX = GetParam("BorderX");
  size = paramBorderX.GetSize();
  if(m_debug)std::cout << "sizex : " << size << std::endl;
  for(i = 0; i<size; i++){
  	BorderX.push_back( paramBorderX[i].GetDouble() );
  }
  paramBorderY = GetParam("BorderY");
  size = paramBorderY.GetSize();
  if(m_debug)std::cout << "sizey : " << size << std::endl;
  for(i = 0; i<size; i++){
  	BorderY.push_back( paramBorderY[i].GetDouble() );
  }

  //param = GetParam("XYBins");
  // read map to the vector of map
  Vec_Map.clear();
  xtotBins.clear();
  ytotBins.clear();
  for(int m = 1; m<paramBorderX.GetSize(); m++){     
    param = GetParam(Form("XYBins%i",m));
    
    size = param.GetSize();
    
    if(m_debug)std::cout << "map infor : " << size << std::endl;
    fBinMap.clear();
  	fXBins.clear();
  	fYBins.clear();
  	for(i = 0; i < size/2; ++i)
  	{
      paramXBins = param[i*2];
      sizeXBins = paramXBins.GetSize();
      paramYBins = param[i*2 + 1];
      sizeYBins = paramYBins.GetSize();

      for(j = 0; j < sizeXBins; ++j){        
        for(k = 0; k < sizeYBins; ++k){
          fBinMap[paramXBins[j].GetDouble()].insert(paramYBins[k].GetDouble());
        }
      }      
  	}//end loop

    
    
    Vec_Map.push_back( fBinMap );	
    
  	// for better performance we transform map of sets to parallel vectors:
  	// vector< double > and vector< vector< double >* >
  	for(itXBin = fBinMap.begin(); itXBin != fBinMap.end(); ++itXBin){
      fXBins.push_back(itXBin->first);
      if(m_debug)std::cout<<m_head<<"now pushing back xBin"<<itXBin->first<<std::endl;
      YBins = new vector< double >(itXBin->second.size());
      fYBins.push_back(YBins);
      YBins->clear();
      for(itYBin = itXBin->second.begin(); itYBin != itXBin->second.end(); ++itYBin){
        YBins->push_back(*itYBin);
        if(m_debug)std::cout<<m_head<<"pushed back ybin "<<(*itYBin)<<std::endl;
        
      }
  	}
    xtotBins.push_back( fXBins );
    ytotBins.push_back( fYBins );
  }
  // get Ecal Z position.
  m_zEcal = GetDouble("ZECal",12650.5);
  
  // read energy fractions for different particles
  param = GetParam("EnergyFraction");
  size = param.GetSize();
  // set default energy fractions values
  fFractionMap.clear();
  fFractionMap[0] = make_pair(0.0, 1.0);

  for(i = 0; i < size/2; ++i)
  {
    paramFractions = param[i*2 + 1];

    ecalFraction = paramFractions[0].GetDouble();
    hcalFraction = paramFractions[1].GetDouble();

    fFractionMap[param[i*2].GetInt()] = make_pair(ecalFraction, hcalFraction);
  }

  // read min E value for timing measurement in ECAL
  fTimingEnergyMin = GetDouble("TimingEnergyMin",4.);
  // For timing
  // So far this flag needs to be false
  // Curved extrapolation not supported
  fElectronsFromTrack = false;

  // read min E value for towers to be saved
  fECalEnergyMin = GetDouble("ECalEnergyMin", 0.0);
  fHCalEnergyMin = GetDouble("HCalEnergyMin", 0.0);

  fECalEnergySignificanceMin = GetDouble("ECalEnergySignificanceMin", 0.0);
  fHCalEnergySignificanceMin = GetDouble("HCalEnergySignificanceMin", 0.0);

  // switch on or off the dithering of the center of calorimeter towers
  fSmearTowerCenter = GetBool("SmearTowerCenter", true);

  // read resolution formulas
  fECalResolutionFormula->Compile(GetString("ECalResolutionFormula", "0"));
  fHCalResolutionFormula->Compile(GetString("HCalResolutionFormula", "0"));

  // import array with output from other modules
  fParticleInputArray = ImportArray(GetString("ParticleInputArray", "ParticlePropagator/particles"));
  fItParticleInputArray = fParticleInputArray->MakeIterator();

  fTrackInputArray = ImportArray(GetString("TrackInputArray", "ParticlePropagator/tracks"));
  fItTrackInputArray = fTrackInputArray->MakeIterator();

  // create output arrays
  fTowerOutputArray = ExportArray(GetString("TowerOutputArray", "towers"));
  fPhotonOutputArray = ExportArray(GetString("PhotonOutputArray", "photons"));

  fEFlowTrackOutputArray = ExportArray(GetString("EFlowTrackOutputArray", "eflowTracks"));
  fEFlowPhotonOutputArray = ExportArray(GetString("EFlowPhotonOutputArray", "eflowPhotons"));
  fEFlowNeutralHadronOutputArray = ExportArray(GetString("EFlowNeutralHadronOutputArray", "eflowNeutralHadrons"));
  
}



//------------------------------------------------------------------------------

void CalorimeterLHCb::Finish()
{
  vector< vector< Double_t >* >::iterator itYBin;
  if(fItParticleInputArray) delete fItParticleInputArray;
  if(fItTrackInputArray) delete fItTrackInputArray;
  for(itYBin = fYBins.begin(); itYBin != fYBins.end(); ++itYBin)
  {
    delete *itYBin;
  }

}

//------------------------------------------------------------------------------

void CalorimeterLHCb::Process()
{
 
  if(m_debug)std::cout<<m_head<<"-------------- Processing Calorimeter ---------------------"<<std::endl;
  
  Candidate *particle, *track;
  TLorentzVector position, momentum;
  Short_t xBin, yBin, flags;
  Int_t number;
  Long64_t towerHit, towerXY, hitXY;
  Double_t ecalFraction, hcalFraction;
  Double_t ecalEnergy, hcalEnergy;
  Double_t ecalSigma, hcalSigma;
  Double_t energyGuess;
  Int_t pdgCode;
  Int_t region = 0;//region of the LHCb calorimeter we're talking about



  TFractionMap::iterator itFractionMap;
  
  vector< Double_t >::iterator itXBin;
  vector< Double_t >::iterator itYBin;
  vector< Double_t > *yBins;

  vector< Long64_t >::iterator itTowerHits;

  DelphesFactory *factory = GetFactory();
  fTowerHits.clear();
  fECalTowerFractions.clear();
  fHCalTowerFractions.clear();
  fECalTrackFractions.clear();
  fHCalTrackFractions.clear();

  // loop over all particles
  fItParticleInputArray->Reset();
  number = -1;
  if(m_debug)std::cout<<m_head<<"---------------- Loop on stable particles --------------------"<<std::endl;
  
  while((particle = static_cast<Candidate*>(fItParticleInputArray->Next()))){
    //cout<<m_head<<"got particle "<<particle->PID<<" with px,py,pz,e "<<particle->Momentum.Px()<<","
      //  <<particle->Momentum.Py()<<","<<particle->Momentum.Pz()<<","<<particle->Momentum.E()<<"with m1,m2"<<particle->M1<<","<<particle->M2<<endl;
    
    region = 0;
    const TLorentzVector& particlePosition = particle->Position;
    ++number;
    //project the particles to the correct z position
    double x = particlePosition.X();
    double y = particlePosition.Y();
    if(m_debug)std::cout<<m_head<<"check particleZ"<<std::endl;    
    double z = particlePosition.Z();
    double t = particlePosition.T();    
    
    if(fabs(z-m_zEcal)>1e-10){
      if(m_debug)std::cout<<m_head<<"particle not proopagated to ecal! fixing"<<std::endl;      
      double tx = particle->Momentum.Px()/particle->Momentum.Pz();
      double ty = particle->Momentum.Py()/particle->Momentum.Pz();
      double e = particle->Momentum.E();
      Double_t c_light = 2.99792458E11;
      Double_t gammam = e / (c_light*c_light);
      Double_t tz = gammam / (particle->Momentum.Pz()) * (m_zEcal-z);
      x+=tx*(m_zEcal-z);
      y+=ty*(m_zEcal-z);
      z=m_zEcal;      
      t+=tz;
    }
    
    if(m_debug)std::cout<<m_head<<"now looking at particle posiiton ("<<x<<","<<y<<","<<z<<"), with PID "<<particle->PID
                        <<", M1 "<<particle->M1<<" and M2 "<<particle->M2<<std::endl;    
    //find the region in the calo we are in.
    for( int i=0; i < BorderX.size(); i++){//this will break if regions are not of the same size.
      if( (fabs(x)<BorderX[i+1] && fabs(y)<BorderY[i+1] ) 
          && !( (fabs(x)<=BorderX[i] && fabs(y)<=BorderY[i] ) ) ){
        region = i+1;
      }
    }
    if (region == 0){
      continue;
    }    
    pdgCode = TMath::Abs(particle->PID);
    
    itFractionMap = fFractionMap.find(pdgCode);
    if(itFractionMap == fFractionMap.end()){
      itFractionMap = fFractionMap.find(0);
    }
    
    ecalFraction = itFractionMap->second.first;
    hcalFraction = itFractionMap->second.second;
    if(m_debug)std::cout<<m_head<<"Using particle PID "<<particle->PID<<", Key "<<particle->M1<<", using ecal fraction "<<ecalFraction<<"  and hcal fraction"<<hcalFraction<<std::endl;
    
    fECalTowerFractions.push_back(ecalFraction);
    fHCalTowerFractions.push_back(hcalFraction);

    if(ecalFraction < 1.0E-9 && hcalFraction < 1.0E-9) continue;
    if(m_debug)std::cout<<m_head<<"has non-zero ecal or hcal fraction"<<std::endl;
    //compute the bin in x where the particle is. given the way the ecal is constructed, it's an array of x positions, and for
    //each x position, there is an array of y positions.
    if(m_debug)std::cout<<m_head<<"checking xregion finding"<<std::endl;
    
    itXBin = lower_bound(xtotBins[region-1].begin(), xtotBins[region-1].end(), x);
    if(itXBin == xtotBins[region-1].begin() || itXBin == xtotBins[region-1].end()) continue;
    xBin = distance(xtotBins[region-1].begin(), itXBin);

    // y bins for given x bin
    yBins = ytotBins[region-1][xBin];

    // find y bin [1, xBins.size - 1]
    itYBin = lower_bound(yBins->begin(), yBins->end(), y);
    if(itYBin == yBins->begin() || itYBin == yBins->end()) continue;
    yBin = distance(yBins->begin(), itYBin);
    
    //found the bins, now make the flags.
    flags = 0;
    flags |= (pdgCode == 11 || pdgCode == 22) << 1;

    // make tower hit {16-bits for eta bin number, 16-bits for phi bin number, 8-bits for flags, 24-bits for particle number}
    towerHit = (Long64_t(xBin) << 48) | (Long64_t(yBin) << 32) | (Long64_t(flags) << 24) | (Long64_t(region) << 16) | Long64_t(number);

    fTowerHits.push_back(towerHit);
  }

  // loop over all tracks
  fItTrackInputArray->Reset();
  number = -1;
  while((track = static_cast<Candidate*>(fItTrackInputArray->Next())))
  {
    region = 0;
    const TLorentzVector& trackPosition = track->Position;
    ++number;

    //define LHCb specific position.
    double x = trackPosition.X();
    double y = trackPosition.Y();    
    double z = trackPosition.Z();
    if(m_debug)std::cout<<m_head<<"check trackZ "<<z<<std::endl;      
    double t = trackPosition.T();
//    if(z!=m_zEcal){
    if(fabs(z-m_zEcal)>1e-10){
      if(m_debug)std::cout<<m_head<<"testing track->Momentum"<<endl;      
      if(m_debug)std::cout<<m_head<<"track not proopagated to ecal! fixing"<<std::endl;      
      double tx = track->Momentum.Px()/track->Momentum.Pz();
      double ty = track->Momentum.Py()/track->Momentum.Pz();
      double e = track->Momentum.E();
      Double_t c_light = 2.99792458E11;
      Double_t gammam = e / (c_light*c_light);
      Double_t tz = gammam / (track->Momentum.Pz()) * (m_zEcal-z);
      x+=tx*(m_zEcal-z);
      y+=ty*(m_zEcal-z);
      z=m_zEcal;
      t+=tz;
      if(m_debug)std::cout<<m_head<<"done fixing"<<std::endl;
      
    }
    //if(m_debug)std::cout<<m_head<<"now looking at track posiiton ("<<x<<","<<y<<","<<z<<"), with M1 "<<track->M1<<", and M2 "<<track->M2<<std::endl;    
    for( int i=1; i < BorderX.size(); i++){
      if( (fabs(x)<BorderX[i] && fabs(y)<BorderY[i] ) && !( (fabs(x)<=BorderX[i-1] && fabs(y)<=BorderY[i-1] ) ) ){  
        region = i;
      }
    }
    if (region == 0){
      continue;
    }



    pdgCode = TMath::Abs(track->PID);

    itFractionMap = fFractionMap.find(pdgCode);
    if(itFractionMap == fFractionMap.end())
    {
      itFractionMap = fFractionMap.find(0);
    }

    ecalFraction = itFractionMap->second.first;
    hcalFraction = itFractionMap->second.second;

    fECalTrackFractions.push_back(ecalFraction);
    fHCalTrackFractions.push_back(hcalFraction);

    // find x bin [1, size - 1]
    itXBin = lower_bound(xtotBins[region-1].begin(), xtotBins[region-1].end(),x);
    if(itXBin == xtotBins[region-1].begin() || itXBin == xtotBins[region-1].end()) continue;
    xBin = distance(xtotBins[region-1].begin(), itXBin);

    // y bins for given x bin
    yBins = ytotBins[region-1][xBin];

    // find x bin [1, size - 1]
    itYBin = lower_bound(yBins->begin(), yBins->end(), y);
    if(itYBin == yBins->begin() || itYBin == yBins->end()) continue;
    yBin = distance(yBins->begin(), itYBin);

    flags = 1;



    // make tower hit {16-bits for eta bin number, 16-bits for phi bin number, 8-bits for flags, 24-bits for track number}
    towerHit = (Long64_t(xBin) << 48) | (Long64_t(yBin) << 32) | (Long64_t(flags) << 24) | (Long64_t(region) << 16) | Long64_t(number);

    fTowerHits.push_back(towerHit);
  }

  // all hits are sorted first by eta bin number, then by phi bin number,
  // then by flags and then by particle or track number
  sort(fTowerHits.begin(), fTowerHits.end());

  // loop over all hits
  towerXY = 0;
  fTower = 0;
  for(itTowerHits = fTowerHits.begin(); itTowerHits != fTowerHits.end(); ++itTowerHits)
  {
    towerHit = (*itTowerHits);
    flags = (towerHit >> 24) & 0x00000000000000FFLL;
    region = (towerHit >> 16) & 0x00000000000000FFLL;
    number = (towerHit) & 0x000000000000FFFFLL;
    hitXY = towerHit >> 32;
    //if(m_debug)std::cout<<m_head<<"towerXY = "<<towerXY<<", hitXY = "<<hitXY<<std::endl;
    
    if(towerXY != hitXY){
      // switch to next tower
      towerXY = hitXY;

      // finalize previous tower
      FinalizeTower();

      // create new tower
      fTower = factory->NewCandidate();

      yBin = (towerHit >> 32) & 0x000000000000FFFFLL;
      xBin = (towerHit >> 48) & 0x000000000000FFFFLL;

      // y bins for given x bin
      yBins = ytotBins[region-1][xBin];



      // calculate x,y of the tower's center
      fTowerX = 0.5*(xtotBins[region-1][xBin - 1] + xtotBins[region-1][xBin]);
      fTowerY = 0.5*((*yBins)[yBin - 1] + (*yBins)[yBin]);
      
      fTowerEta =  -0.5 * log( tan( ( atan2( sqrt(fTowerX*fTowerX+fTowerY*fTowerY),m_zEcal ) ) /2.));

      fTowerEdges[0] = xtotBins[region-1][xBin - 1];
      fTowerEdges[1] = xtotBins[region-1][xBin];
      fTowerEdges[2] = (*yBins)[yBin - 1];
      fTowerEdges[3] = (*yBins)[yBin];

      fECalTowerEnergy = 0.0;
      fHCalTowerEnergy = 0.0;

      fECalTrackEnergy = 0.0;
      fHCalTrackEnergy = 0.0;
      
      fECalTrackSigma = 0.0;
      fHCalTrackSigma = 0.0;
      
      fTowerTrackHits = 0;
      fTowerPhotonHits = 0;

      fECalTowerTrackArray->Clear();
      fHCalTowerTrackArray->Clear();


    }

    // check for track hits
    if(flags & 1){
      ++fTowerTrackHits;
      
      track = static_cast<Candidate*>(fTrackInputArray->At(number));
      momentum = track->Momentum;
      position = track->Position;
      double x = position.X();
      double y = position.Y();    
      double z = position.Z();
      if(m_debug)std::cout<<m_head<<"check trackZ "<<z<<std::endl;      
      double t = position.T();
      
      if(fabs(z-m_zEcal)>1e-10){
        if(m_debug)std::cout<<m_head<<"testing track->Momentum"<<endl;      
        if(m_debug)std::cout<<m_head<<"track not proopagated to ecal! fixing"<<std::endl;      
        double tx = momentum.Px()/momentum.Pz();
        double ty = momentum.Py()/momentum.Pz();
        double e = momentum.E();
        Double_t c_light = 2.99792458E11;
        Double_t gammam = e / (c_light*c_light);
        Double_t tz = gammam / (momentum.Pz()) * (m_zEcal-z);
        x+=tx*(m_zEcal-z);
        y+=ty*(m_zEcal-z);
        z=m_zEcal;
        t+=tz;
        if(m_debug)std::cout<<m_head<<"done fixing"<<std::endl;
        
      }

      
      ecalEnergy = momentum.E() * fECalTrackFractions[number];
      hcalEnergy = momentum.E() * fHCalTrackFractions[number];
      
      if(ecalEnergy > fTimingEnergyMin && fTower)
      {
        if(fElectronsFromTrack)
        {
          fTower->ECalEnergyTimePairs.push_back(make_pair<Float_t, Float_t>(ecalEnergy, track->Position.T()));
          fTower->CellsInCluster.push_back(std::make_tuple(x, y, ecalEnergy));          
        }
      }

      if(fECalTrackFractions[number] > 1.0E-9 && fHCalTrackFractions[number] < 1.0E-9)
      {
        fECalTrackEnergy += ecalEnergy;
        ecalSigma = fECalResolutionFormula->Eval(0.0, fTowerEta , 0.0, momentum.E());
        if(ecalSigma/momentum.E() < track->TrackResolution) energyGuess = ecalEnergy;        
        else energyGuess = momentum.E();

        fECalTrackSigma += (track->TrackResolution)*energyGuess*(track->TrackResolution)*energyGuess;
        //Candidate *particle1;
        fECalTowerTrackArray->Add(track);
      }
     
      else if(fECalTrackFractions[number] < 1.0E-9 && fHCalTrackFractions[number] > 1.0E-9)
      {
        fHCalTrackEnergy += hcalEnergy;
        hcalSigma = fHCalResolutionFormula->Eval(0.0, fTowerEta, 0.0, momentum.E());
        if(hcalSigma/momentum.E() < track->TrackResolution) energyGuess = hcalEnergy;
        else energyGuess = momentum.E();

        fHCalTrackSigma += (track->TrackResolution)*energyGuess*(track->TrackResolution)*energyGuess;
        fHCalTowerTrackArray->Add(track);
      }
      
      else if(fECalTrackFractions[number] < 1.0E-9 && fHCalTrackFractions[number] < 1.0E-9)
      {
        fEFlowTrackOutputArray->Add(track);
      }

      continue;
    }

    // check for photon and electron hits in current tower
    if(flags & 2) ++fTowerPhotonHits;

    particle = static_cast<Candidate*>(fParticleInputArray->At(number));
    momentum = particle->Momentum;
    position = particle->Position;
    //define LHCb specific position.
    double x = position.X();
    double y = position.Y();    
    double z = position.Z();
    double t = position.T();
    
    if(fabs(z-m_zEcal)>1e-10){
      if(m_debug)std::cout<<m_head<<"testing track->Momentum"<<endl;      
      if(m_debug)std::cout<<m_head<<"track not proopagated to ecal! fixing"<<std::endl;      
      double tx = momentum.Px()/momentum.Pz();
      double ty = momentum.Py()/momentum.Pz();
      double e = momentum.E();
      Double_t c_light = 2.99792458E11;
      Double_t gammam = e / (c_light*c_light);
      Double_t tz = gammam / (momentum.Pz()) * (m_zEcal-z);
      x+=tx*(m_zEcal-z);
      y+=ty*(m_zEcal-z);
      z=m_zEcal;
      t+=tz;
            
    }
    // fill current tower
    ecalEnergy = momentum.E() * fECalTowerFractions[number];
    hcalEnergy = momentum.E() * fHCalTowerFractions[number];

    fECalTowerEnergy += ecalEnergy;
    fHCalTowerEnergy += hcalEnergy;

    if(ecalEnergy > fTimingEnergyMin && fTower)
    {
      if (abs(particle->PID) != 11 || !fElectronsFromTrack)
      {
        
        fTower->ECalEnergyTimePairs.push_back(make_pair<Float_t, Float_t>(ecalEnergy, particle->Position.T()));
        fTower->CellsInCluster.push_back(std::make_tuple(x, y, ecalEnergy));
        fTower->SetUniqueID( particle->M1 );
        fTower->M2 = particle->M2;
        
        fTower->M1 = particle->M1;	  
        
      }
    }
    
    if(m_debug)std::cout<<m_head<<"*************************************"<<std::endl;
    
    if(m_debug)std::cout<<m_head<<"for particle "<<particle->PID<<", with status "<<particle->Status<<", M1 = "<<particle->M1<<", M2 = "<<particle->M2<<std::endl;
    if(m_debug)std::cout<<m_head<<" for tower, have"<<fTower->PID<<", with status "<<fTower->Status<<", M1 = "<<fTower->M1<<", M2 = "<<fTower->M2<<std::endl;
    
    fTower->AddCandidate(particle);
  }

  // finalize last tower
  FinalizeTower();
}

//------------------------------------------------------------------------------

void CalorimeterLHCb::FinalizeTower()
{
  Candidate *track, *tower, *mother;
  Double_t energy, pt, x, y, eta, phi;
  Double_t ecalEnergy, hcalEnergy;
  Double_t ecalNeutralEnergy, hcalNeutralEnergy;
  
  Double_t ecalSigma, hcalSigma;
  Double_t ecalNeutralSigma, hcalNeutralSigma;

  Double_t weightTrack, weightCalo, bestEnergyEstimate, rescaleFactor;
  
  TLorentzVector momentum;
  TFractionMap::iterator itFractionMap;

  Float_t weight, sumWeightedTime, sumWeight;
  Float_t sumWeightedX, sumWeightedY;
  
  if(!fTower) return;

  ecalSigma = fECalResolutionFormula->Eval(0.0, fTowerEta, 0.0, fECalTowerEnergy);
  hcalSigma = fHCalResolutionFormula->Eval(0.0, fTowerEta, 0.0, fHCalTowerEnergy);

  ecalEnergy = LogNormal(fECalTowerEnergy, ecalSigma);
  hcalEnergy = LogNormal(fHCalTowerEnergy, hcalSigma);

  ecalSigma = fECalResolutionFormula->Eval(0.0, fTowerEta, 0.0, ecalEnergy);
  hcalSigma = fHCalResolutionFormula->Eval(0.0, fTowerEta, 0.0, hcalEnergy);

  if(ecalEnergy < fECalEnergyMin || ecalEnergy < fECalEnergySignificanceMin*ecalSigma) ecalEnergy = 0.0;
  if(hcalEnergy < fHCalEnergyMin || hcalEnergy < fHCalEnergySignificanceMin*hcalSigma) hcalEnergy = 0.0;

  energy = ecalEnergy + hcalEnergy;

  if(!fSmearTowerCenter){
    //smear by one cell size
    //    x = gRandom->Uniform(fTowerEdges[0], fTowerEdges[1]);
    //    y = gRandom->Uniform(fTowerEdges[2], fTowerEdges[3]);
    // use bin center
    //bug. Should return MC particle position here.
    x = fTowerX;
    y = fTowerY;
  }
  else
  {
  //smear half cell size around real position
  //calculate real position
    sumWeight = 0.0;
    sumWeightedX = 0.0;
    sumWeightedY = 0.0;
    if(fTower->CellsInCluster.size()<=0) {
      x = gRandom->Uniform(fTowerEdges[0], fTowerEdges[1]);
      y = gRandom->Uniform(fTowerEdges[2], fTowerEdges[3]);             
    } else {
      for(size_t i=0; i < fTower->CellsInCluster.size(); ++i) {
        weight = std::get<2>(fTower->CellsInCluster[i]);
        sumWeight += weight;
        sumWeightedX += weight * std::get<0>(fTower->CellsInCluster[i]);
        sumWeightedY += weight * std::get<1>(fTower->CellsInCluster[i]);           
      }  
      Float_t x0 = sumWeightedX/sumWeight;
      Float_t y0 = sumWeightedY/sumWeight;
//      if(m_debug)std::cout << "photon position tower bounday" << x0 << " " << y0 << " (x " << fTowerEdges[0] << ", " << fTowerEdges[1] << ") " << 
//              "(y" << fTowerEdges[2] << "," << fTowerEdges[3] << ")"<< std::endl;;
    //why (x0, y0) is not inside of tower?
      if(x0>=fTowerEdges[0]&&x0<fTowerEdges[1]&&y0>=fTowerEdges[2]&&y0<fTowerEdges[3]) {
        while(1) {

          x = gRandom->Uniform(-0.25,0.25)*(fTowerEdges[1]-fTowerEdges[0]) + x0;
          y = gRandom->Uniform(-0.25,0.25)*(fTowerEdges[3]-fTowerEdges[2]) + y0;
          if(x>=fTowerEdges[0]&&x<fTowerEdges[1]&&y>=fTowerEdges[2]&&y<fTowerEdges[3]) break;
        }
      } else {
          x = gRandom->Uniform(fTowerEdges[0], fTowerEdges[1]);
          y = gRandom->Uniform(fTowerEdges[2], fTowerEdges[3]);                 
      }
    }
  }

  // Time calculation for tower
  fTower->NTimeHits = 0;
  sumWeightedTime = 0.0;
  sumWeight = 0.0;

  for(size_t i = 0; i < fTower->ECalEnergyTimePairs.size(); ++i)
  {
    //    if(m_debug)std::cout << "The time in ECal: " << fTower->ECalEnergyTimePairs[i].second << std::endl;
    weight = TMath::Sqrt(fTower->ECalEnergyTimePairs[i].first);
    sumWeightedTime += weight * fTower->ECalEnergyTimePairs[i].second;
    sumWeight += weight;
    fTower->NTimeHits++;
  }


  TLorentzVector *position = new TLorentzVector( x, y, m_zEcal, sumWeightedTime/sumWeight);
  //eta = -0.5 * log( tan( ( atan( sqrt(x*x+y*y)/m_zEcal ) ) /2));
  eta = position->Eta();
  phi = position->Phi();
  pt = energy / TMath::CosH(eta);
  




  if(sumWeight > 0.0)
  {
    fTower->Position.SetPtEtaPhiE(1.0, eta, phi, sumWeightedTime/sumWeight);
  }
  else
  {
    fTower->Position.SetPtEtaPhiE(1.0, eta, phi, 999999.9);
  }


  fTower->Momentum.SetPtEtaPhiE(pt, eta, phi, energy);
  fTower->Eem = ecalEnergy;
  fTower->Ehad = hcalEnergy;

  fTower->Edges[0] = fTowerEdges[0];
  fTower->Edges[1] = fTowerEdges[1];
  fTower->Edges[2] = fTowerEdges[2];
  fTower->Edges[3] = fTowerEdges[3];

  if(energy > 0.0)
  {
    if(fTowerPhotonHits > 0 && fTowerTrackHits == 0)
    {
      fPhotonOutputArray->Add(fTower);
    }

    fTowerOutputArray->Add(fTower);
  }
  
  // fill energy flow candidates
  fECalTrackSigma = TMath::Sqrt(fECalTrackSigma);
  fHCalTrackSigma = TMath::Sqrt(fHCalTrackSigma);

  //compute neutral excesses
  ecalNeutralEnergy = max( (ecalEnergy - fECalTrackEnergy) , 0.0);
  hcalNeutralEnergy = max( (hcalEnergy - fHCalTrackEnergy) , 0.0);
  
  ecalNeutralSigma = ecalNeutralEnergy / TMath::Sqrt(fECalTrackSigma*fECalTrackSigma + ecalSigma*ecalSigma);
  hcalNeutralSigma = hcalNeutralEnergy / TMath::Sqrt(fHCalTrackSigma*fHCalTrackSigma + hcalSigma*hcalSigma);
  
   // if ecal neutral excess is significant, simply create neutral EflowPhoton tower and clone each track into eflowtrack
  if(ecalNeutralEnergy > fECalEnergyMin && ecalNeutralSigma > fECalEnergySignificanceMin)
  {
    // create new photon tower
    tower = static_cast<Candidate*>(fTower->Clone());


    tower->M2 = fTower->M2;
    tower->SetUniqueID( fTower->GetUniqueID() );
    tower->M1 = fTower->M1;    
    


    pt =  ecalNeutralEnergy / TMath::CosH(eta);

    tower->Momentum.SetPtEtaPhiE(pt, eta, phi, ecalNeutralEnergy);
    tower->Eem = ecalNeutralEnergy;
    tower->Ehad = 0.0;
    tower->PID = 22;



    tower->Position = *position;


    fEFlowPhotonOutputArray->Add(tower);
    // if(m_debug)std::cout
    // <<"PID"<<"\t"
    // <<"Status"<<"\t"
    // <<"Px"<<"\t"
    // <<"Py"<<"\t"
    // <<"Pz"<<"\t"
    // <<"Mass"<<"\t"
    // <<"Energy"<<"\t"
    // <<"x"<<"\t"
    // <<"y"<<"\t"
    // <<"z"<<"\t"
    // <<"t"<<"\t"
    // <<"M1"<<"\t"
    // <<"M2"<<"\t"
    // <<"UniqueID"<<"\t"
    // <<std::endl;
    // if(m_debug)std::cout
    //   <<tower->PID<<"\t"
    //   <<tower->Status<<"\t"
    //   <<tower->Momentum.Px()<<"\t"
    //   <<tower->Momentum.Py()<<"\t"
    //   <<tower->Momentum.Pz()<<"\t"
    //   <<tower->Mass<<"\t"
    //   <<ecalNeutralEnergy<<"\t"
    //   <<tower->Position.X()<<"\t"
    //   <<tower->Position.Y()<<"\t"
    //   <<tower->Position.Z()<<"\t"
    //   <<tower->Position.T()<<"\t"
    //   <<tower->M1<<"\t"
    //   <<tower->M2<<"\t"
    //   <<tower->GetUniqueID()<<"\t"
    //   <<std::endl;
    

    //clone tracks
    fItECalTowerTrackArray->Reset();
    while((track = static_cast<Candidate*>(fItECalTowerTrackArray->Next())))
    {
      mother = track;
      track = static_cast<Candidate*>(track->Clone());
      track->AddCandidate(mother);

      fEFlowTrackOutputArray->Add(track);
    }
  
  }
 
  // if neutral excess is not significant, rescale eflow tracks, such that the total charged equals the best measurement given by the calorimeter and tracking
  else if(fECalTrackEnergy > 0.0)
  {
    weightTrack = (fECalTrackSigma > 0.0) ? 1 / (fECalTrackSigma*fECalTrackSigma) : 0.0;
    weightCalo  = (ecalSigma > 0.0) ? 1 / (ecalSigma*ecalSigma) : 0.0;
  
    bestEnergyEstimate = (weightTrack*fECalTrackEnergy + weightCalo*ecalEnergy) / (weightTrack + weightCalo); 
    rescaleFactor = bestEnergyEstimate/fECalTrackEnergy;

    //rescale tracks
    fItECalTowerTrackArray->Reset();
    while((track = static_cast<Candidate*>(fItECalTowerTrackArray->Next())))
    {  
      mother = track;
      track = static_cast<Candidate*>(track->Clone());
      track->AddCandidate(mother);

      track->Momentum *= rescaleFactor;
      fEFlowTrackOutputArray->Add(track);
    }
  }


  // if hcal neutral excess is significant, simply create neutral EflowNeutralHadron tower and clone each track into eflowtrack
  if(hcalNeutralEnergy > fHCalEnergyMin && hcalNeutralSigma > fHCalEnergySignificanceMin)
  {
    // create new photon tower
    tower = static_cast<Candidate*>(fTower->Clone());
    pt =  hcalNeutralEnergy / TMath::CosH(eta);
    
    tower->Momentum.SetPtEtaPhiE(pt, eta, phi, hcalNeutralEnergy);
    tower->Ehad = hcalNeutralEnergy;
    tower->Eem = 0.0;
    
    fEFlowNeutralHadronOutputArray->Add(tower);
   
    //clone tracks
    fItHCalTowerTrackArray->Reset();
    while((track = static_cast<Candidate*>(fItHCalTowerTrackArray->Next())))
    {
      mother = track;
      track = static_cast<Candidate*>(track->Clone());
      track->AddCandidate(mother);

      fEFlowTrackOutputArray->Add(track);
    }
  
  }
 
  // if neutral excess is not significant, rescale eflow tracks, such that the total charged equals the best measurement given by the calorimeter and tracking
  else if(fHCalTrackEnergy > 0.0)
  {
    weightTrack = (fHCalTrackSigma > 0.0) ? 1 / (fHCalTrackSigma*fHCalTrackSigma) : 0.0;
    weightCalo  = (hcalSigma > 0.0) ? 1 / (hcalSigma*hcalSigma) : 0.0;
  
    bestEnergyEstimate = (weightTrack*fHCalTrackEnergy + weightCalo*hcalEnergy) / (weightTrack + weightCalo); 
    rescaleFactor = bestEnergyEstimate / fHCalTrackEnergy;

    //rescale tracks
    fItHCalTowerTrackArray->Reset();
    while((track = static_cast<Candidate*>(fItHCalTowerTrackArray->Next())))
    {  
      mother = track;
      track = static_cast<Candidate*>(track->Clone());
      track->AddCandidate(mother);

      track->Momentum *= rescaleFactor;

      fEFlowTrackOutputArray->Add(track);
    }
  }
  
  
}

//------------------------------------------------------------------------------

Double_t CalorimeterLHCb::LogNormal(Double_t mean, Double_t sigma)
{
  Double_t a, b;

  if(mean > 0.0)
  {
    b = TMath::Sqrt(TMath::Log((1.0 + (sigma*sigma)/(mean*mean))));
    a = TMath::Log(mean) - 0.5*b*b;

    return TMath::Exp(a + b*gRandom->Gaus(0.0, 1.0));
  }
  else
  {
    return 0.0;
  }
}
