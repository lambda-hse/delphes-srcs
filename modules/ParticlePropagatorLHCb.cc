/*
 *  Delphes: a framework for fast simulation of a generic collider experiment
 *  Copyright (C) 2012-2014  Universite catholique de Louvain (UCL), Belgium
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/** \class ParticlePropagatorLHCb
 *
 *  Propagates charged and neutral particles
 *  from a given vertex to a cylinder defined by its radius,
 *  its half-length, centered at (0,0,0) and with its axis
 *  oriented along the z-axis.
 *
 *  AD: 2018-1-29: Modified to include all particle types within 
 *  LHCb acceptance
 *
 *  \author B. Siddi - INFN Ferrara/CERN (2015-2017)
 *  
 */

#include "modules/ParticlePropagatorLHCb.h"

#include "classes/DelphesClasses.h"
#include "classes/DelphesFactory.h"
#include "classes/DelphesFormula.h"



#include "ExRootAnalysis/ExRootResult.h"
#include "ExRootAnalysis/ExRootFilter.h"
#include "ExRootAnalysis/ExRootClassifier.h"

#include "TMath.h"
#include "TString.h"
#include "TFormula.h"
#include "TRandom3.h"
#include "TObjArray.h"
#include "TDatabasePDG.h"
#include "TLorentzVector.h"
#include "TBox.h"

#include <algorithm>
#include <stdexcept>
#include <iostream>
#include <sstream>

using namespace std;

//------------------------------------------------------------------------------

ParticlePropagatorLHCb::ParticlePropagatorLHCb() :
  fItInputArray(0)
{
}

//------------------------------------------------------------------------------

ParticlePropagatorLHCb::~ParticlePropagatorLHCb()
{
}

//------------------------------------------------------------------------------

void ParticlePropagatorLHCb::Init()
{

  m_head  = "ParticlePropagatorLHCb";
  
  m_debug = GetBool("Debug",0);
  
  fRadius = GetDouble("Radius", 1.0);
  fRadius2 = fRadius*fRadius;
  fHalfLength = GetDouble("HalfLength", 3.0);
  fBz = GetDouble("Bz", 0.0);
  //allow configurable T/FT station z positions
  fzT1 = GetDouble("zT1",7700.0);
  fzT2 = GetDouble("zT2",8400.0);
  fzT3 = GetDouble("zT3",9400.0);
  //configurable magnet kick and center (just in case)
  fPtKick = GetDouble("PtKick",1200.0);
  fZmagCenter = GetDouble("ZmagCenter",5300.0);
  fFormulaZmag = GetString("FormulaZMagCenter","");
  
  fXminPropTo = GetDouble("xMinPropTo",100);
  fXmaxPropTo = GetDouble("xMaxPropTo",6000);
  fYminPropTo = GetDouble("yMinPropTo",100);
  fYmaxPropTo = GetDouble("yMaxPropTo",3000);
  fZPropTo = GetDouble("zPropTo",12500);
  
  if(fRadius < 1.0E-2)
  {
    if(m_debug){std::cout<<m_head<<"ERROR: magnetic field radius is too low\n"<<std::endl; }
    return;
  }
  if(fHalfLength < 1.0E-2)
  {
    if(m_debug){std::cout<<m_head<<"ERROR: magnetic field length is too low\n"<<std::endl; }
    return;
  }

  // import array with output from filter/classifier module

  fInputArray = ImportArray(GetString("InputArray", "Delphes/stableParticles"));
  fItInputArray = fInputArray->MakeIterator();

  // create output arrays
  fOutputArray = ExportArray(GetString("OutputArray", "stableParticles"));
  fOutputArrayUpstream = ExportArray(GetString("OutputArrayUpstream","upstreamParticles"));
  fOutputArrayDownstream = ExportArray(GetString("OutputArrayDownstream","downstreamParticles"));

  fChargedHadronOutputArray = ExportArray(GetString("ChargedHadronOutputArray", "chargedHadrons"));
  fChargedHadronOutputUpstreamArray = ExportArray(GetString("ChargedHadronOutputUpstreamArray", "upstreamchargedHadrons"));
  fChargedHadronOutputDownstreamArray = ExportArray(GetString("ChargedHadronOutputDownstreamArray", "downstreamchargedHadrons"));

  fElectronOutputArray = ExportArray(GetString("ElectronOutputArray", "electrons"));
  fElectronOutputUpstreamArray = ExportArray(GetString("ElectronOutputUpstreamArray", "upstreamelectrons"));
  fElectronOutputDownstreamArray = ExportArray(GetString("ElectronOutputDownstreamArray", "downstreamelectrons"));

  fMuonOutputArray = ExportArray(GetString("MuonOutputArray", "muons"));
  fMuonOutputUpstreamArray = ExportArray(GetString("MuonOutputUpstreamArray", "upstreammuons"));
  fMuonOutputDownstreamArray = ExportArray(GetString("MuonOutputDownstreamArray", "downstreammuons"));

  fNeutralOutputArray = ExportArray(GetString("NeutralOutputArray","neutrals"));
  fNeutralOutputUpstreamArray = ExportArray(GetString("NeutralOutputUpstreamArray","upstreamneutrals"));
  fNeutralOutputDownstreamArray = ExportArray(GetString("NeutralOutputDownstreamArray","downstreamneutrals"));

  //separate photons from other neutrals to allow conversions.
  fPhotonOutputArray = ExportArray(GetString("PhotonOutputArray","photons"));
  fPhotonOutputUpstreamArray = ExportArray(GetString("PhotonOutputUpstreamArray","upstreamphotons"));
  fPhotonOutputDownstreamArray = ExportArray(GetString("PhotonOutputDownstreamArray","downstreamphotons"));
  
}

//------------------------------------------------------------------------------

void ParticlePropagatorLHCb::Finish()
{
  if(fItInputArray) delete fItInputArray;
}

//------------------------------------------------------------------------------

void ParticlePropagatorLHCb::Process()
{
  Candidate *candidate, *mother;
  TLorentzVector candidatePosition, candidateMomentum;
  Double_t px, py, pz, pt, pt2, e, q, p, qOverP;
  Double_t x, y, z, t, r, phi;
  Double_t x_c, y_c, r_c, phi_c, phi_0;
  Double_t x_t, y_t, z_t, r_t;
  Double_t t1, t2, t3, t4, t5, t6;
  Double_t t_z, t_r, t_ra, t_rb;
  Double_t tmp, discr, discr2;
  Double_t delta, gammam, omega, asinrho;
  Double_t rcu, rc2, dxy, xd, yd, zd;

  TFormula zCenterFormula;
  
  Double_t z1 = fzT1;//7700.0;
  Double_t z2 = fzT2;//8400.0;
  //Double_t z3 = 9100.0;
  Double_t z3 = fzT3;//9400.0;
  // Double_t z3 = (fZPropTo>fzT3) ? fZPropTo : fzT3;//9400.0;
  
  TBox *inner = new TBox(-12.E-3,-12.E-3,12.E-3,12.E-3);
  TBox *outer = new TBox(-300.E-3,-250.E-3,300.E-3,250.E-3);
  
  
  //const Double_t c_light = 2.99792458E8;

  fItInputArray->Reset();
  //loop over candidates
  Double_t zCenterMagnet = fZmagCenter;//possible function of tx,ty,q/p
  Bool_t hasFormula =  zCenterFormula.Compile(fFormulaZmag.c_str());
    
  while((candidate = static_cast<Candidate*>(fItInputArray->Next()))){
    Bool_t isUpStream = 0;
    Bool_t isDownStream = 0;
    Bool_t isLong = 0;
    
    candidatePosition = candidate->Position;
    candidateMomentum = candidate->Momentum;
    x = candidatePosition.X();
    y = candidatePosition.Y();
    z = candidatePosition.Z();
    q = candidate->Charge;

    px = candidateMomentum.Px();
    py = candidateMomentum.Py();
    pz = candidateMomentum.Pz();
    pt = candidateMomentum.Pt();
    p = candidateMomentum.P();
    pt2 = candidateMomentum.Perp2();
    e = candidateMomentum.E();

    qOverP = q/p;
    
    Double_t tx = px / pz;
    Double_t ty = py / pz;
    
    Double_t xk = x;
    Double_t yk = y;
    
    Double_t px2 = px*px;
    Double_t py2 = py*py;
    Double_t pz2 = pz*pz;
    Double_t p = candidateMomentum.P();
    Double_t c_light = 2.99792458E11;
    Double_t gammam = e / (c_light*c_light);
    
    
    // Shift of the IT hole
    Double_t xPShift = 0.0; // mm
    Double_t xMShift = 0.0; // mm
    
    //ptKick
    Double_t ptKick = 1.0e-9 < fabs(q) ? fPtKick : 0.;//if we have a charged track, apply the ptKick.
    
    
    // is the final state particle produced in the LHCb Detector "box" of allowed angles?
    if((outer->IsInside(tx,ty)) && !(inner->IsInside(tx,ty))){ isUpStream = true; }
    else{ continue; }
    
    if(pt2 < 1.0E-9){ continue; }///have some pt
    
    if(pz<0) continue;//no backward tracks

    if(!hasFormula) zCenterMagnet = zCenterFormula.Eval(qOverP);
    
    // if(fZPropTo<zCenterMagnet){
    //   z3 = fZPropTo;
    //   fBz = 0.0;     
    // }

    
    if(TMath::Abs(fBz) < 1.0E-9){//magnet off
      //track related quantities
      Double_t tz = gammam / (pz) * (-z + z3);
      tx = px / pz;
      ty = py / pz; //added
      //no kick
      xk += tx* (z3 - z);
      yk += ty * (z3 - z);
      TVector3 *pos = new TVector3(xk,yk,z3);
      //pos->RotateX(0.03601);

      Double_t xa = x  + px/gammam * tz;
      Double_t ya = y + py/gammam * tz;
   

      mother = candidate;
      candidate = static_cast<Candidate*>(candidate->Clone());

      candidate->Position.SetXYZT(xk, yk, z3, candidatePosition.T() + tz);

      candidate->Momentum = candidateMomentum;
      candidate->AddCandidate(mother);

      fOutputArray->Add(mother);
      if(TMath::Abs(q) > 1.0E-9){
        switch(TMath::Abs(candidate->PID)){
        case 11:
          fElectronOutputArray->Add(candidate);
          break;
        case 13:
          fMuonOutputArray->Add(candidate);
          break;
        default:
          fChargedHadronOutputArray->Add(candidate);
        }
      }
      else{//add neutral candidates to their own array.
        switch(TMath::Abs(candidate->PID)){
        case 22:
          fPhotonOutputArray->Add(candidate);
          break;          
        default:
          fNeutralOutputArray->Add(candidate);        
        }
      }
      
    }        
    else{
      Double_t tz = gammam / (pz) * (-z + zCenterMagnet);
      tx = px / pz;
      ty = py / pz; 
      xk += tx * (zCenterMagnet - z);
      yk += ty * (zCenterMagnet - z);
      TVector3 *pos = new TVector3(xk,yk,zCenterMagnet);
                                                                                                                                    
      Double_t xa = x  + px/gammam * tz;
      Double_t ya = y + py/gammam * tz;
   
      pos->SetXYZ(xa,ya,zCenterMagnet);
      //kick the particle. Make sure the sign is right for charged particles.
      if(((candidate->Charge > 0) && (fBz > 0)) || ((candidate->Charge < 0) && (fBz < 0))){ ptKick = -fPtKick; }
      px += ptKick;
      px2 = px*px;
      //conserve momentum
      pz2 = TMath::Power(p,2) - py2 - px2;
      
      //no imaginary momentum
      if(pz2>0){ pz = TMath::Sqrt(pz2); }
      else{ pz = 0.0; }
      
      tx = px / pz;
      ty = py / pz;
      
      tz += gammam / (pz) * (z1 - zCenterMagnet);
      
      tx = px / pz;
      ty = py / pz; 
      
      xk += tx * (z1 - zCenterMagnet);
      yk += ty * (z1 - zCenterMagnet);
       
      pos->SetXYZ(xk,yk,z1);
      
      //ask if we are now in t1 acceptance.
      
      Bool_t recT1 = ((TMath::Abs(pos->X())>100.0) || (TMath::Abs(pos->Y())>100.0)) &&
        ((TMath::Abs(pos->X())<3000.0) && (TMath::Abs(pos->Y())< 2500.0));
      
      xk += tx * (z2 -z1);
      yk += ty * (z2 - z1);
      tz += gammam / (pz) * (z2 - z1);
      pos->SetXYZ(xk,yk,z2);
      
      Bool_t recT2 = ((TMath::Abs(pos->X())>100.0) || (TMath::Abs(pos->Y())>100.0)) &&
        ((TMath::Abs(pos->X())<3000.0) && (TMath::Abs(pos->Y())< 2500.0));
     
      xk += tx*(z3-z2);
      yk += ty*(z3-z2);
      pos->SetXYZ(xk,yk,z3);
      tz += gammam / (pz) * (z3 - z2);
      Double_t theta = TMath::ASin(pt/p);

      Bool_t recT3 = ((TMath::Abs(pos->X())>100.0) || (TMath::Abs(pos->Y())>100.0)) &&
        ((TMath::Abs(pos->X())<3000.0) && (TMath::Abs(pos->Y())< 2500.0));

      if(fZPropTo>fzT3)
      {
        xk += tx*(fZPropTo-z3);
        yk += ty*(fZPropTo-z3);
        pos->SetXYZ(xk,yk,fZPropTo);
        tz += gammam / (pz) * (fZPropTo - z3);
        Double_t theta = TMath::ASin(pt/p);
        
      }
      
      Bool_t recPropTo = ((TMath::Abs(pos->X())>fXminPropTo) || (TMath::Abs(pos->Y())>fYminPropTo)) &&
        ((TMath::Abs(pos->X())<fXmaxPropTo) && (TMath::Abs(pos->Y())< fYmaxPropTo));
      
      candidateMomentum.SetTheta(theta);
      candidateMomentum.SetPxPyPzE(px,py,pz,e);


      if(recT1 && recT2 && recT3) { isDownStream = true; }
      
      if(isUpStream && isDownStream && recPropTo) {isLong = true; }

      if(isLong){ //not quite a long track, just in the acceptance the whole time.
        mother = candidate;
        candidate = static_cast<Candidate*>(candidate->Clone());
        candidate->Position.SetXYZT(pos->X(), pos->Y(), pos->Z(), candidatePosition.T() + tz);

        candidate->Momentum = candidateMomentum;
        //candidate->Dxy = dxy*1.0E3;
        candidate->Xd = xd*1.0E3;
        candidate->Yd = yd*1.0E3;
        candidate->Zd = zd*1.0E3;

        candidate->AddCandidate(mother);

        fOutputArray->Add(mother);
        switch(TMath::Abs(candidate->PID))
        {
        case 11://electron
          fElectronOutputArray->Add(mother);
          break;
        case 13://muon
          fMuonOutputArray->Add(mother);
          break;
        case 22://gamma
          fPhotonOutputArray->Add(candidate);          
          break;
        case 111://pi0
        case 2112: //neutron
          fNeutralOutputArray->Add(candidate);
          break;
        default:
          fChargedHadronOutputArray->Add(mother);
        }
      }

      if(isUpStream && !isLong) {
        mother = candidate;
        candidate = static_cast<Candidate*>(candidate->Clone());
        candidate->Position.SetXYZT(pos->X(), pos->Y(), pos->Z(), candidatePosition.T() + tz);
        candidate->Momentum = candidateMomentum;
        candidate->AddCandidate(mother);
        fOutputArrayUpstream->Add(mother);
        switch(TMath::Abs(candidate->PID))
        {
        case 11:
          fElectronOutputUpstreamArray->Add(mother);
          break;
        case 13:
          fMuonOutputUpstreamArray->Add(mother);
          break;
        case 22://gamma
          fPhotonOutputUpstreamArray->Add(candidate);
          break;
        case 111://pi0
        case 2112: //neutron
          fNeutralOutputUpstreamArray->Add(candidate);
          break;
        default:
          fChargedHadronOutputUpstreamArray->Add(mother);
        }
      }
      
      if(isDownStream && !isLong)
      {
        mother = candidate;  
        candidate = static_cast<Candidate*>(candidate->Clone());
        candidate->Position.SetXYZT(pos->X(), pos->Y(), pos->Z(), candidatePosition.T() + tz);
        candidate->Momentum = candidateMomentum;
        //candidate->Dxy = dxy*1.0E3;
        candidate->Xd = xd*1.0E3;
        candidate->Yd = yd*1.0E3;
        candidate->Zd = zd*1.0E3;
        candidate->AddCandidate(mother);
        fOutputArrayDownstream->Add(candidate);
        switch(TMath::Abs(candidate->PID))
        {
        case 11:
          fElectronOutputDownstreamArray->Add(mother);
          break;
        case 13:
          fMuonOutputDownstreamArray->Add(mother);
          break;
        case 22://gamma
          fPhotonOutputDownstreamArray->Add(candidate);
          break;
        case 111://pi0
        case 2112: //neutron
          fNeutralOutputDownstreamArray->Add(candidate);
          break;
        default:
          fChargedHadronOutputDownstreamArray->Add(mother);
        }
      }
    }
  }
}

