/*
 *  Delphes: a framework for fast simulation of a generic collider experiment
 *  Copyright (C) 2012-2014  Universite catholique de Louvain (UCL), Belgium
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/** \class Efficiency1D
 *
 *  Selects candidates from the InputArray according to the efficiency formula.
 *
 *  \author B. Siddi - INFN Ferrara/CERN (2015-2017)
 *
 */

#include "modules/Efficiency1D.h"

#include "classes/DelphesClasses.h"
#include "classes/DelphesFactory.h"
#include "classes/DelphesFormula.h"

#include "ExRootAnalysis/ExRootResult.h"
#include "ExRootAnalysis/ExRootFilter.h"
#include "ExRootAnalysis/ExRootClassifier.h"

#include "TMath.h"
#include "TString.h"
#include "TFormula.h"
#include "TRandom3.h"
#include "TObjArray.h"
#include "TDatabasePDG.h"
#include "TLorentzVector.h"

#include <algorithm> 
#include <stdexcept>
#include <iostream>
#include <sstream>

#include "TH3.h"
#include "TH2.h"
#include "TROOT.h"

using namespace std;

//------------------------------------------------------------------------------

Efficiency1D::Efficiency1D() :
  fFileHistoName(0), fItInputArray(0),
  fHisto_num(0), fHisto_den(0), fFileHisto(0), fhNum(0), fhDen(0), fhEff(0),
  fhEff_hx(0), fhEff_hy(0), fhEff_hz(0), fhDenominator_hx(0), fhDenominator_hy(0), fhDenominator_hz(0)
{
  // fEff = new DelphesFormula;
}

//------------------------------------------------------------------------------

Efficiency1D::~Efficiency1D()
{
  // if(fhNum) delete fhNum;
  // if(fhDen) delete fhDen;
  if(fFileHisto) delete fFileHisto;
  if(fFileHistoName) delete fFileHistoName;
  
  if(fHisto_num) delete fHisto_num;
  
  if(fHisto_den) delete fHisto_den;
  
  // if(fhEff_hx) delete fhEff_hx;
  // if(fhEff_hy) delete fhEff_hy;
  // if(fhEff_hz) delete fhEff_hz;
  // if(fhDenominator_hx) delete fhDenominator_hx;
  // if(fhDenominator_hy) delete fhDenominator_hy;
  // if(fhDenominator_hz) delete fhDenominator_hz;
  
  
}

//------------------------------------------------------------------------------

void Efficiency1D::Init()
{
  // read efficiency Histo

  fFileHistoName = GetString("Efficiency1D", "ResSparse.root");
  fHisto_num = GetString("NumeratorHisto","hTxTyP_Delphes");
  fHisto_den = GetString("DenominatorHisto","hTxTyP_Recoble");
  

  std::cout<<"file: "<<fFileHistoName<<std::endl;
  fFileHisto = new TFile(fFileHistoName,"OPEN");  

  fhNum = (TH1D*)fFileHisto->Get(fHisto_num);
  fhDen = (TH1D*)fFileHisto->Get(fHisto_den);
  if(fhNum) std::cout<<"get Num histo"<<std::endl;
  if(fhDen) std::cout<<"get Den histo"<<std::endl;
  
  fhNum->SetDirectory(0);
  fhDen->SetDirectory(0);
  
  fhEff_hx = (TH1D*)fhNum->Clone("fhEff_hx");

  fhDenominator_hx = (TH1D*)fhDen->Clone("fhDenominator_hx");

  fhDenominator_hx->SetDirectory(0);

  fhEff_hx->SetDirectory(0);
  
  fhEff_hx->Divide(fhDenominator_hx);

  fhEff = (TH1D*)fhNum->Clone("fhEff");
  fhEff->Divide(fhNum,fhDen,1,1);
  fhEff->SetDirectory(0);
  
  
  fFileHisto->Close();

  // import input array

  fInputArray = ImportArray(GetString("InputArray", "ParticlePropagator/stableParticles"));
  fItInputArray = fInputArray->MakeIterator();

  // create output array

  fOutputArray = ExportArray(GetString("OutputArray", "stableParticles"));
}

//------------------------------------------------------------------------------

void Efficiency1D::Finish()
{
  if(fItInputArray) delete fItInputArray;
}

//------------------------------------------------------------------------------

void Efficiency1D::Process()
{ 
  Candidate *candidate;
  
  fItInputArray->Reset();
  while((candidate = static_cast<Candidate*>(fItInputArray->Next())))
  {
    const TLorentzVector &candidatePosition = candidate->Position;

    Double_t x = candidatePosition.X();
    Double_t y = candidatePosition.Y();
    r = std::sqrt( x*x+ y*y );

    Int_t xbin = fhEff_hx->FindBin(r);
 
    Double_t xcont = fhEff_hx->GetBinContent(xbin);

    double rnd = gRandom->Uniform();
    
    if(rnd > xcont) continue;
    
    fOutputArray->Add(candidate);
  }
}

//------------------------------------------------------------------------------
