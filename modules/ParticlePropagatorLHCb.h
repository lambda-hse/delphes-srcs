/*
 *  Delphes: a framework for fast simulation of a generic collider experiment
 *  Copyright (C) 2012-2014  Universite catholique de Louvain (UCL), Belgium
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ParticlePropagatorLHCb_h
#define ParticlePropagatorLHCb_h

/** \class ParticlePropagatorLHCb
 *
 *  Propagates charged and neutral particles
 *  from a given vertex to a cylinder defined by its radius, 
 *  its half-length, centered at (0,0,0) and with its axis
 *  oriented along the z-axis.
 *
 *  \author B. Siddi - INFN Ferrara/CERN (2015-2017)
 *
 */

#include "classes/DelphesModule.h"

class TClonesArray;
class TIterator;

class ParticlePropagatorLHCb: public DelphesModule
{
public:

  ParticlePropagatorLHCb();
  ~ParticlePropagatorLHCb();

  void Init();
  void Process();
  void Finish();

private:
  
  std::string m_head;
  
  bool m_debug;
  //necessary numbers for the correct sim of the LHCb Geometry  
  Double_t fRadius, fRadius2, fHalfLength;
  Double_t fBz;
  //z positions of relevant detectors
  Double_t fzT1;
  Double_t fzT2;
  Double_t fzT3;
  Double_t fPtKick;
  Double_t fZmagCenter;
  Double_t fZPropTo;
  Double_t fXminPropTo, fXmaxPropTo, fYminPropTo, fYmaxPropTo;
  
  
  std::string fFormulaZmag;
  

  TIterator *fItInputArray; //!

  const TObjArray *fInputArray; //!

  TObjArray *fOutputArray; //!
  TObjArray *fOutputArrayUpstream; //!
  TObjArray *fOutputArrayDownstream; //!
  
  
  TObjArray *fChargedHadronOutputArray; //!
  TObjArray *fChargedHadronOutputUpstreamArray; //!
  TObjArray *fChargedHadronOutputDownstreamArray; //!

  TObjArray *fElectronOutputArray; //!
  TObjArray *fElectronOutputUpstreamArray; //!
  TObjArray *fElectronOutputDownstreamArray; //!

  TObjArray *fMuonOutputArray; //!
  TObjArray *fMuonOutputUpstreamArray; //!
  TObjArray *fMuonOutputDownstreamArray; //!

  TObjArray *fNeutralOutputArray;
  TObjArray *fNeutralOutputUpstreamArray;
  TObjArray *fNeutralOutputDownstreamArray;


  TObjArray *fPhotonOutputArray;
  TObjArray *fPhotonOutputUpstreamArray;
  TObjArray *fPhotonOutputDownstreamArray;

  ClassDef(ParticlePropagatorLHCb, 1)
};

#endif
