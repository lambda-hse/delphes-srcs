/*
 *  Delphes: a framework for fast simulation of a generic collider experiment
 *  Copyright (C) 2012-2014  Universite catholique de Louvain (UCL), Belgium
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/** \class MomentumSmearingHisto
 *
 *  Performs transverse momentum resolution smearing.
 *
 *  \author B. Siddi - INFN Ferrara/CERN (2015-2017)
 *
 */

#include "modules/MomentumSmearingHisto.h"

#include "classes/DelphesClasses.h"
#include "classes/DelphesFactory.h"
#include "classes/DelphesFormula.h"

#include "ExRootAnalysis/ExRootResult.h"
#include "ExRootAnalysis/ExRootFilter.h"
#include "ExRootAnalysis/ExRootClassifier.h"

#include "TMath.h"
#include "TString.h"
#include "TFormula.h"
#include "TRandom3.h"
#include "TObjArray.h"
#include "TDatabasePDG.h"
#include "TLorentzVector.h"

#include <algorithm> 
#include <stdexcept>
#include <iostream>
#include <sstream>

#include "TAxis.h"
#include "TH1.h"
#include "TROOT.h"
using namespace std;

//------------------------------------------------------------------------------

MomentumSmearingHisto::MomentumSmearingHisto() :
  fHistoFile(0), fHisto(0), fItInputArray(0)
{
  // fFormula = new char;
}

//------------------------------------------------------------------------------

MomentumSmearingHisto::~MomentumSmearingHisto()
{
  if(fHistoFile) delete fHistoFile;
  if(fHisto) delete fHisto;
  if (fRes) delete fRes;
  if (hsRes) delete hsRes;
  
}

//------------------------------------------------------------------------------

void MomentumSmearingHisto::Init()
{
  // read resolution formula
  m_head = "MomentumSmearingHisto";
  m_debug = GetBool("Debug",0);
  

  // fHistoFile = GetString("ResolutionHisto", "Res.root");
  fHistoFile = GetString("ResolutionHistoFile", "/afs/cern.ch/user/b/bsiddi/cmtuser/Brunel_v50r0/options/Res.root");
  fHisto = GetString("ResolutionHisto", "sparseRes");
  if(m_debug){std::cout<<m_head<<"\t file:"<<fHistoFile<<std::endl;}
  
  fRes = new TFile(fHistoFile,"OPEN");
  hsRes = (THnSparseD*)fRes->Get(fHisto);
  
  // import input array

  fInputArray = ImportArray(GetString("InputArray", "ParticlePropagator/stableParticles"));
  fItInputArray = fInputArray->MakeIterator();

  // create output array

  fOutputArray = ExportArray(GetString("OutputArray", "stableParticles"));
}

//------------------------------------------------------------------------------

void MomentumSmearingHisto::Finish()
{
  if(fItInputArray) delete fItInputArray;
}

//------------------------------------------------------------------------------

void MomentumSmearingHisto::Process()
{
  Candidate *candidate, *mother;
  Double_t pt, eta, phi, e;
  Double_t tx,ty,p,pz;
  Double_t txRMS, tyRMS, pRMS;

  Int_t bin0min;
  Int_t bin0max;
  Int_t bin1min;
  Int_t bin1max;
  Int_t bin2min;
  Int_t bin2max;
  
  fItInputArray->Reset();
  while((candidate = static_cast<Candidate*>(fItInputArray->Next())))
  {
    const TLorentzVector &candidatePosition = candidate->Position;
    const TLorentzVector &candidateMomentum = candidate->Momentum;
    // eta = candidatePosition.Eta();
    // phi = candidatePosition.Phi();
    // pt = candidateMomentum.Pt();
    e = candidateMomentum.E();
    
    tx = candidateMomentum.Px() / candidateMomentum.Pz();
    ty = candidateMomentum.Py() / candidateMomentum.Pz();
    p = candidateMomentum.P()/1000.;
    pz = candidateMomentum.Pz()/1000.;
    

    // apply smearing formula
    Double_t epsilon = 0.00005;
    
    TAxis *axis0 = (TAxis*)hsRes->GetAxis(0);
    TAxis *axis1 = (TAxis*)hsRes->GetAxis(1);
    TAxis *axis2 = (TAxis*)hsRes->GetAxis(2);
    
    bin0min = axis0->FindBin(tx-epsilon);
    bin0max = axis0->FindBin(tx+epsilon);
    bin1min = axis1->FindBin(ty-epsilon);
    bin1max = axis1->FindBin(ty+epsilon);
    bin2min = axis2->FindBin(p-epsilon);    
    bin2max = axis2->FindBin(p+epsilon);
    
    axis0->SetRange(bin0min,bin0max);    
    axis1->SetRange(bin1min,bin1max);
    axis2->SetRange(bin2min,bin2max);
    

    TH1D *h1txRMS = (TH1D*)hsRes->Projection(3);
    TH1D *h1tyRMS = (TH1D*)hsRes->Projection(4);
    TH1D *h1pRMS  = (TH1D*)hsRes->Projection(5);
    
    h1txRMS->SetName("h1txRMS");
    h1tyRMS->SetName("h1tyRMS");
    h1pRMS->SetName("h1pRMS");

    txRMS = h1txRMS->GetRMS();
    tyRMS = h1tyRMS->GetRMS();
    pRMS  = h1pRMS->GetRMS();

    
    tx = gRandom->Gaus(tx, txRMS);
    ty = gRandom->Gaus(ty, tyRMS);
    p  = gRandom->Gaus(p,  pRMS);
    
    // if(pt <= 0.0) continue;

    mother = candidate;
    candidate = static_cast<Candidate*>(candidate->Clone());
    // eta = candidateMomentum.Eta();
    // phi = candidateMomentum.Phi();
    candidate->Momentum.SetPxPyPzE(tx*pz*1000.,ty*pz*1000.,pz*1000.,e);
    
    // candidate->Momentum.SetPtEtaPhiE(pt, eta, phi, pt*TMath::CosH(eta));
    // candidate->TrackResolution = pRMS*1000;
    
    candidate->AddCandidate(mother);
        
    fOutputArray->Add(candidate);    
  }
}

//------------------------------------------------------------------------------
